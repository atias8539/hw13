#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <mutex>
class Server

{
public:
	Server();
	~Server();
	void serve(int port);
	void clientHandler(SOCKET clientSocket);
	std::vector <std::string> connectedUsers;
	std::vector<SOCKET> socketsVector;
	std::queue<std::string> fileQueue;
	void accept();
	std::mutex mtx;
private:
	void enterStringToQueue(std::string data);
	void getStringfromQueue();

	std::string getTextOfFile();
	void changeFileTxt(std::string data);
	int numberOfPlace(std::string name);
	std::string getNameNext(std::string name);
	SOCKET _serverSocket;
};

