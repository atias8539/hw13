#include "server.h"
#include <exception>
#include <iostream>
#include <string>
#include "helper.h"
#include <thread>
#include <chrono>


#include <algorithm>
using namespace std::this_thread;     // sleep_for, sleep_until
using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
using std::chrono::system_clock;
Server::Server()
{
	
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{

	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	socketsVector.push_back(client_socket);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	std::thread first(&Server::clientHandler,this,client_socket);
	first.detach();
	//clientHandler(client_socket);
}


void Server::clientHandler(SOCKET clientSocket)
{
	
	
	try
	{
		int runs = 0;
		Helper main;
		int code = 0;
		int size = 0;
		std::string retData;
		std::string dataToFile;
		std::string nameCurr;
		SOCKET tempSocket;
		retData = main.getStringPartFromSocket(clientSocket, 100);
		code = std::stoi(retData.substr(0, 3).c_str());
		bool niv = true;
		std::string tempName;
		size = std::stoi(retData.substr(2, 3).c_str());
		nameCurr = retData.substr(5, size);
		connectedUsers.push_back(nameCurr);
		while(niv)
		{
				switch (code)
				{
				case MT_CLIENT_LOG_IN:
						main.sendUpdateMessageToClient(clientSocket, getTextOfFile(),connectedUsers.front(), getNameNext(connectedUsers.front()), numberOfPlace(nameCurr));
					break;
				case MT_CLIENT_UPDATE:
					size = std::stoi(retData.substr(5, 3).c_str());
					dataToFile = retData.substr(8, size).c_str();
					/*
					part of mutex- nobody understand really what to do- there are functions that works
					but i dont know if this is what you want

					mtx.lock();
					enterStringToQueue(dataToFile);
					getStringfromQueue();
					mtx.unlock();*/
					changeFileTxt(dataToFile);
					for (size_t i = 0; i < socketsVector.size(); i++)
					{
						main.sendUpdateMessageToClient(socketsVector[i], getTextOfFile(), connectedUsers.front(), getNameNext(connectedUsers.front()), i + 1);

					}

					break;
				case MT_CLIENT_FINISH:
					std::cout << retData;
					size = std::stoi(retData.substr(5, 3).c_str());
					dataToFile = retData.substr(8, size).c_str();
					
					/*
					part of mutex- nobody understand really what to do- there are functions that works
					but i dont know if this is what you want

					mtx.lock();
					enterStringToQueue(dataToFile);
					getStringfromQueue();
					mtx.unlock();*/
					changeFileTxt(dataToFile);
					tempName = connectedUsers.front();
					connectedUsers.erase(connectedUsers.begin());
					connectedUsers.push_back(tempName);
					tempSocket = clientSocket;
					socketsVector.erase(socketsVector.begin());
					socketsVector.push_back(tempSocket);
					for (size_t i = 0; i < socketsVector.size(); i++)
					{
						main.sendUpdateMessageToClient(socketsVector[i], getTextOfFile(), connectedUsers.front(), getNameNext(connectedUsers.front()), i+1);
					}
					break;
				case MT_CLIENT_EXIT:
					std::cout << "quit";
					connectedUsers.erase(std::remove(connectedUsers.begin(), connectedUsers.end(), nameCurr), connectedUsers.end());
					socketsVector.erase(std::remove(socketsVector.begin(), socketsVector.end(), clientSocket), socketsVector.end());
					for (size_t i = 0; i < socketsVector.size(); i++)
					{
						main.sendUpdateMessageToClient(socketsVector[i], getTextOfFile(), connectedUsers.front(), getNameNext(connectedUsers.front()), i + 1);
					}
					niv = false;
					break;
				default:
					break;
				}
				if (niv)
				{
					retData = main.getStringPartFromSocket(clientSocket, 100);
					code = std::stoi(retData.substr(0, 3).c_str());
				}
			
		
		}
		std::cout << "finish";
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}


}
std::string Server::getTextOfFile()
{
	std::ifstream file("shared_doc.txt"); // open this file for input
	std::string data;

	std::string line;
	while (std::getline(file, line)) // for each line read from the file
	{
		data = data +"\n"+ line;
	}
	return data;
}
void Server::changeFileTxt(std::string data)
{
	std::ifstream file;
	std::ofstream outfile;

	file.open("shared_doc.txt");
	outfile.open("newData.txt");
	outfile << data;
	outfile.close();
	file.close();
	remove("shared_doc.txt");
	rename("newData.txt", "shared_doc.txt");

}
int Server::numberOfPlace(std::string name)
{
	int place = 0,j=0;
	for (int i = 0; i < connectedUsers.size(); i++)
	{
		if (connectedUsers[i] == name)
		{
			i = connectedUsers.size();
		}
		else
		{
			j++;
		}
	}
	place = j +1 ;
	return place;
}
std::string Server::getNameNext(std::string name)
{
	std::string nameRet;
	if (connectedUsers.size() == 1)
	{
		nameRet = "null";
	}
	else if (connectedUsers.back() == name)
	{
		nameRet = connectedUsers.front();
	}
	else
	{
		for (int i = 0; i < connectedUsers.size(); i++)
		{
			if (connectedUsers[i] == name)
			{
				nameRet = connectedUsers[i+1];
			}

		}
	}
	return nameRet;
}
void Server::enterStringToQueue(std::string data)
{
	std::string line;
	data = data + "\n";
	for (size_t i = 0; i < data.length(); i++)
	{
		if (data[i]!='\n')
		{
			line = line + data[i];
		}
		else
		{
			fileQueue.push(line);
			line = "";
		}
	}
}
void Server::getStringfromQueue()
{
	std::string line;
	std::cout << "size:" << fileQueue.size();
	for (size_t i = 0; i < fileQueue.size(); i++)
	{
		std::cout << "\n\nline:" << fileQueue.front() << "\n\n";
		fileQueue.pop();
		std::cout << "size:" << fileQueue.size()<<"\n";

	}
	changeFileTxt(line);
}